const gulp = require('gulp');
const sass = require('gulp-sass');
const gulppug = require('gulp-pug');

const pug = require('gulp-pug');


const browserSync = require('browser-sync').create();




function pugToHtml() {
    return gulp.src('src/pug/**/*.pug')
        .pipe(pug({ pretty: true })) // Устанавливаем опцию pretty в true
        //.pipe(pug())
        .pipe(gulp.dest('src'))
        .pipe(browserSync.stream());
}



//compile scss into css
function style() {
    return gulp.src('src/scss/**/*.scss')
        .pipe(sass().on('error',sass.logError))
        .pipe(gulp.dest('src/css'))
        .pipe(browserSync.stream());
}
function watch() {
    browserSync.init({
        server: {
            baseDir: "./src",
            index: "/index.html"
        }
    });
    gulp.watch('src/scss/**/*.scss', style);
    gulp.watch('src/pug/**/*.pug', pugToHtml);
    gulp.watch('./*.html').on('change', browserSync.reload);
    gulp.watch('./js/**/*.js').on('change', browserSync.reload);
}


exports.style = style;
exports.watch = watch;
exports.pugToHtml = pugToHtml;